/* docCookies function made by Mozilla */
var docCookies = {
	getItem: function (sKey) {
		if (!sKey) { return null; }
		return decodeURIComponent(document.cookie.replace(new RegExp("(?:(?:^|.*;)\\s*" + encodeURIComponent(sKey).replace(/[\-\.\+\*]/g, "\\$&") + "\\s*\\=\\s*([^;]*).*$)|^.*$"), "$1")) || null;
	},
	setItem: function (sKey, sValue, vEnd, sPath, sDomain, bSecure) {
		if (!sKey || /^(?:expires|max\-age|path|domain|secure)$/i.test(sKey)) { return false; }
		var sExpires = "";
		if (vEnd) {
			switch (vEnd.constructor) {
				case Number:
					sExpires = vEnd === Infinity ? "; expires=Fri, 31 Dec 9999 23:59:59 GMT" : "; max-age=" + vEnd;
					break;
				case String:
					sExpires = "; expires=" + vEnd;
					break;
				case Date:
					sExpires = "; expires=" + vEnd.toUTCString();
					break;
			}
		}
		document.cookie = encodeURIComponent(sKey) + "=" + encodeURIComponent(sValue) + sExpires + (sDomain ? "; domain=" + sDomain : "") + (sPath ? "; path=" + sPath : "") + (bSecure ? "; secure" : "");
		return true;
	},
	removeItem: function (sKey, sPath, sDomain) {
		if (!this.hasItem(sKey)) { return false; }
		document.cookie = encodeURIComponent(sKey) + "=; expires=Thu, 01 Jan 1970 00:00:00 GMT" + (sDomain ? "; domain=" + sDomain : "") + (sPath ? "; path=" + sPath : "");
		return true;
	},
	hasItem: function (sKey) {
		if (!sKey) { return false; }
		return (new RegExp("(?:^|;\\s*)" + encodeURIComponent(sKey).replace(/[\-\.\+\*]/g, "\\$&") + "\\s*\\=")).test(document.cookie);
	},
	keys: function () {
		var aKeys = document.cookie.replace(/((?:^|\s*;)[^\=]+)(?=;|$)|^\s*|\s*(?:\=[^;]*)?(?:\1|$)/g, "").split(/\s*(?:\=[^;]*)?;\s*/);
		for (var nLen = aKeys.length, nIdx = 0; nIdx < nLen; nIdx++) { aKeys[nIdx] = decodeURIComponent(aKeys[nIdx]); }
		return aKeys;
	}
};

function save_feedback_to_cookie () {
	message    = document.getElementById('feedback_message').value;
	duplicate  = document.getElementById('feedback_duplicate_to_email').checked;
	email      = document.getElementById('feedback_email').value;
	channels   = document.getElementById('feedback_form').getElementsByClassName('channel');
	channel_id = "";
	for (i=0; i<channels.length; i++) {
		if (channels[i].checked) channel_id = channels[i].value;
	}
	json = '{"message": "' + message + '", "duplicate": "' + duplicate + '", "email": "' + email + '", "channel_id": "' + channel_id + '"}';
	docCookies.setItem('feedback', json, 31536e3, "/");
}

function restore_feedback_from_cookie () {
	try {
		feedback = JSON.parse(docCookies.getItem('feedback'));
	} catch (e) {
		docCookies.removeItem('feedback');
		feedback = null;
	}
	if (feedback) {
		document.getElementById('feedback_message').value = feedback.message;
		document.getElementById('feedback_duplicate_to_email').checked = (feedback.duplicate=="true");
		if (document.getElementById('feedback_email').value == '') document.getElementById('feedback_email').value = feedback.email;
		channels = document.getElementById('feedback_form').getElementsByClassName('channel');
		for (i=0; i<channels.length; i++) {
			if (channels[i].value == feedback.channel_id) channels[i].checked = true;
		}
	}
}

function toggle_account_active_class () {
	element = document.getElementById('id_account_form');
	class_name = 'active';
	if (!element.classList.contains(class_name)) {element.classList.add(class_name);}
	else {element.classList.remove(class_name);}
}

function toggle_comments_moderation (force_on) {
	mod_buttons = docCookies.getItem('mod_buttons');
	if (!mod_buttons || force_on) {
		document.getElementById('toggle_comments_moderation').classList.add('button-success');
		sheet = document.styleSheets[0];
		window.comments_moderation_rule_id = sheet.insertRule('.comments .comment .controls .moderate {display: inline-block !important;}', sheet.cssRules.length);
		docCookies.setItem('mod_buttons', '1', 31536e3, "/");
	}
	else {
		document.getElementById('toggle_comments_moderation').classList.remove('button-success');
		docCookies.setItem('mod_buttons', '', 31536e3, "/");
		sheet = document.styleSheets[0];
		sheet.deleteRule(window.comments_moderation_rule_id);
	}
}

function callAjax (url, method, callback, post) {
	var xmlhttp;
	post = post || '';
	xmlhttp = new XMLHttpRequest();
	xmlhttp.onreadystatechange = function() {
		if (xmlhttp.readyState == 4 && xmlhttp.status == 200) {
			callback(xmlhttp.responseText);
		}
	}
	if (method == 'GET') {
		xmlhttp.open('GET', url, true);
		xmlhttp.send();
	}
	else if (method == 'POST') {
		xmlhttp.open("POST", url, true);
		xmlhttp.setRequestHeader("Content-type","application/x-www-form-urlencoded");
		xmlhttp.setRequestHeader("X-CSRFToken", docCookies.getItem('csrftoken'));
		xmlhttp.send(post);
	}
}

function insertAfter (newNode, referenceNode) {
	referenceNode.parentNode.insertBefore(newNode, referenceNode.nextSibling);
}
