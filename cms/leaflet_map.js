{% load global_tags %}
{% minify_include_js 'js/leaflet.js' %}

// @license [magnet:?xt=urn:btih:0b31508aeb0634b347b8270c7bee4d411b5d4109&dn=agpl-3.0.txt] [GNU AGPL v3]
var DOMReady = function(a, b, c) {
	b = document
	c = 'addEventListener'
	b[c] ? b[c]('DocumentContentLoaded', a) : window.attachEvent('onload', a)
}

var geoData = {
	"type": "Feature",
	"geometry": {
		"type": "LineString",
		"coordinates": {{ waypoints }}
	}
};

function makemap() {
	var map = L.map("{{ title }}", {"center": {{ center }}, "zoom": {{ zoom }}});
	var mapLink = '<a href="https://openstreetmap.org">OpenStreetMap</a>';

	L.tileLayer('https://{s}.tile.openstreetmap.org/{z}/{x}/{y}.png', {
		attribution: mapLink,
		maxZoom: 18,
	}).addTo(map);

	var geoStyle = {
		"color": "#3333ff",
		"weight": 8,
		"opacity": 0.64
	};
	L.geoJson(geoData, { style: geoStyle }).addTo(map);
	L.marker({{ marker }}).addTo(map);
}

DOMReady(makemap());
// @license-end
