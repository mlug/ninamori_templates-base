{% spaceless %}
{% load i18n %}
{% blocktrans with restore_code=restore_code username=username %}Someone made request for account restoration

Here is your code for restoration:
{{ restore_code }}

This code will expire soon, you should act quickly if you want to restore this account.
Just ignore this message if you do not need to restore your account.

Your username is: {{ username }}
Your password is secret{% endblocktrans %}
{% endspaceless %}
